FROM debian:buster

RUN apt-get update && apt-get install python3 \
      python3-pip \
      python3-dev \
      git -y \
      && rm -rf /var/lib/apt/lists/*
RUN pip3 install -U pip setuptools coverage mock
CMD ["bash"]
