# Docker Borescope Artifacts

Official build of borescope  for use in Docker.

## Getting started
To build the image locally:
```bash
cd path_to_Dockerfile
docker build -t borescope-test:v0.1 .
```
Where -t is the name and optionally a tag in the 'name:tag' format
